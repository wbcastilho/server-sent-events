<?php

declare(strict_types=1);

header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');

date_default_timezone_set('America/Sao_Paulo');

$id = 1;

while (true) {
    // Evento date
    echo "id: date-{$id}\n";   
    echo "event: date\n";    
    echo 'data: {"date": "' . date('d/m/Y') . '"}';
    echo "\n\n";    
    // Fim do evento

    // Evento time
    echo "id: time-{$id}\n";
    echo "event: time\n";    
    echo 'data: {"time": "' . date('H:i:s') . '", "value": "' . rand(1, 1000) . '"}';   
    echo "\n\n";    
    // Fim do evento

    // Evento teste
    $data = array(
        'firstName' =>  'Welber',
        'lastName'  =>  'Castilho'
    );

    $str = json_encode($data);
    echo "id: teste-{$id}\n";
    echo "event: teste\n"; 
    echo "data: {$str}\n\n";    
    // Fim do evento

    ob_flush();
    flush();

    // Sai do loop caso a conexão seja fechada
    if (connection_aborted()) break;

    sleep(1);

    $id++;
}