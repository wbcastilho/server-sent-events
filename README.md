# server-sent events
![PHP](https://img.shields.io/badge/php-v7.4%2B-blue)
![GitHub last commit](https://img.shields.io/github/last-commit/AzeemIdrisi/PhoneSploit-Pro?logo=github)

Exemplo Server-Sent Event(SSE) usando php e javascript. 

## :rocket: Começando

### Desabilitar output buffering no php.ini
Para que o SSE funcione em seu servidor PHP é necessário desabilitar o output buffering adicionando a linha abaixo em seu arquivo php.ini
```
output_buffering=Off
```